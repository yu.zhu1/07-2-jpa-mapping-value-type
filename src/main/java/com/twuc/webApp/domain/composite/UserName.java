package com.twuc.webApp.domain.composite;

import javax.persistence.Embeddable;

// TODO
//
// 补充 UserName 的定义，使 UserName 成为一个 value type。其中 firstName的列名称为 first_name，
// 而 lastName 的列名称为 last_name。两列都不能够为 null。并且 firstName 和 lastName 的最大长度
// 均为 64。
// <--start-
@Embeddable
public class UserName {

    private String firstName;
    private String lastName;

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public UserName() {
    }

    public UserName(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }
}
// --end->